var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema ({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index : {type: '2dsphere', sparse:true}
    }

});

bicicletaSchema.statics.createInstnce = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};


bicicletaSchema.methods.toString = function (){
    return 'code: ' + this.code + ' | color: ' + this.color;

};

bicicletaSchema.statics.allBisics = function(cb){
    return this.find({}, cb);

};

model.exports = mongoose.model('Bicicleta', bicicletaSchema);

